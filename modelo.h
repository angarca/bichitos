#ifndef modelo_H
#define modelo_H

namespace clasesGlobales{
	struct Modelo{
		static const String AssetPath;
		enum Estado{ ENPRESENTACION, ENMAPA, ENJUEGO, ENRESUMEN } estado;
		Modelo();
		struct EnPresentacion{
			static const String configuracionName;
			static const String &AssetPath;
			String fondo;
			String mapaName;
			EnPresentacion(Modelo *modelo);
			~EnPresentacion();
		private:
			bool load(const Array<String> &line);
		}*enPresentacion;
		struct EnMapa{
			String fondo;
			struct Pantalla{ String fileName, imageName;};
			Array<Pantalla> pantallas;
			EnMapa(Modelo *modelo);
			~EnMapa();
		}*enMapa;
		struct EnJuego{
			struct Paleta{ String identificador, imageName; Sint32 Animacion, a, Estados, umbralAparicion;
					 bool seleccionable, caible;
				bool amI( const String &id) const { return this->identificador == id;}
			};
			Array<Paleta> paleta;
			String fondo, resumen;
			Sint32 W,H, **grid, **floor, TW, TH, umbralAparicion, puntuacion;
			struct EstadoSeleccion{bool seleccionado; Sint32 orden;}**seleccionados;
			SDL_TimerID timerID;
			EnJuego(Modelo *modelo, const String &fileName);
			~EnJuego();
		}*enJuego;
	};
}

#endif//modelo_H
