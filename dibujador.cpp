#include "cabeceras.h"

const clasesGlobales::Pantalla::Color clasesGlobales::Pantalla::defaultBackground(0x0,0x0,0x0,0xff);

clasesGlobales::Pantalla::Pantalla(Modelo *m):modelo(m){
depuracion("Entrando Pantalla::Pantalla.\n");
	SDL_InitSubSystem(SDL_INIT_VIDEO);
	IMG_Init(IMG_INIT_PNG);	// IMG_INIT_JPG | PNG | TIF | WEBP
	this->ventana = SDL_CreateWindow("Mi ventana SDL",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,0,0,SDL_WINDOW_FULLSCREEN_DESKTOP);
	SDL_GetWindowSize( this->ventana, &(this->W), &(this->H));
	this->renderer = SDL_CreateRenderer( this->ventana, -1, 0);
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY,"linear");
	enPresentacion = new EnPresentacion(this);
	switch(modelo->estado){
	case clasesGlobales::Modelo::ENPRESENTACION:
		enMapa = nullptr;	enJuego = nullptr;	enResumen = nullptr;
		break;
	case clasesGlobales::Modelo::ENMAPA:
		enMapa = new EnMapa(this); enJuego = nullptr;	enResumen = nullptr;
		break;
	case clasesGlobales::Modelo::ENJUEGO:
		enMapa = new EnMapa(this); enJuego = new EnJuego(this); enResumen = nullptr;
		break;
	case clasesGlobales::Modelo::ENRESUMEN:
		enMapa = new EnMapa(this); enJuego = new EnJuego(this); enResumen = new EnResumen(this);
		break;
	default:;
	}
depuracion("Saliendo Pantalla::Pantalla.\n");
}

clasesGlobales::Pantalla::~Pantalla(){
depuracion("Entrando Pantalla::~Pantalla.\n");
	if(enPresentacion) delete enPresentacion;
	if(enMapa) delete enMapa;
	if(enJuego) delete enJuego;
	if(enResumen) delete enResumen;
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(ventana);
	IMG_Quit();
	SDL_QuitSubSystem(SDL_INIT_VIDEO);
depuracion("Saliendo Pantalla::~Pantalla.\n");
}

void funcionesGlobales::dibujadorEnJuego(void *param){	clasesGlobales::Pantalla *pantalla = (clasesGlobales::Pantalla*) param;
	pantalla->enJuego->draw(pantalla);
}
