# README #

This is a simple Jelly Splash-like game (https://play.google.com/store/apps/details?id=com.wooga.jelly_splash&hl=es) written with SDL2 android project template.

To use it, first you need to have installed http://libsdl.org/download-2.0.php under:
/usr/local/include/SDL2
/usr/local/lib

Also uses a personal simple library https://bitbucket.org/angarca/milibs.

Although is designed to run on android, this is just a preview that builds and run on a PC. To test it use:
make todo
./main