#include "cabeceras.h"

using namespace clasesGlobales;
using namespace funcionesGlobales;

int main(int argc, char *argv[]){
Depuracion::iniciarLog();
depuracion("Entrando main.\n");
	Modelo modelo;
	Pantalla pantalla(&modelo);

	despachador(&pantalla);

depuracion("Acabando main.\n");

	exit(0);
	return 0;
}
