#include "cabeceras.h"

clasesGlobales::Modelo::EnMapa::EnMapa(clasesGlobales::Modelo *modelo){
depuracion("Entrando Modelo::EnMapa::EnMapa.\n");
	bool salida = false;	Array<String> line;	clasesGlobales::Modelo::EnMapa::Pantalla pantalla;
	RWops configuracion(modelo->enPresentacion->mapaName);
	while(!salida){
		line = configuracion.readLine().split();
		if(line[0] == "fin") salida = true;
		else if(line[0] == "fondo")
			this->fondo = modelo->AssetPath + line[1];
		else if(!(line[0] == "" || line[1] == "")){
			pantalla.imageName = modelo->AssetPath + line[0];
			pantalla.fileName = modelo->AssetPath + line[1];
			this->pantallas.add(pantalla);
		}// else	// Identificador no reconocido, ignorar.
	}
depuracion("Saliendo Modelo::EnMapa::EnMapa.\n");
}

clasesGlobales::Modelo::EnMapa::~EnMapa(){}
