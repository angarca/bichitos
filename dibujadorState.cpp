#include "cabeceras.h"

void clasesGlobales::Pantalla::State::calcAreas(clasesGlobales::Pantalla *screen){
	Sint32 W = this->background.getW(), H = this->background.getH();
	this->areaDrawable.x = this->areaDrawable.y = 0;
	if( screen->H * W > screen->W * H){
		this->areaDrawable.w = screen->W * H / screen->H;
		this->areaDrawable.h = H;
	}else{
		this->areaDrawable.w = W;
		this->areaDrawable.h = screen->H * W / screen->W;
	}
/*	if( W < this->areaDrawable.w || H < this->areaDrawable.h)
		throw "Error, Pantalla::State::calcAreas: the background isn't big enought to the areaDrawable.\n";*/
			// El tamaño de la pantalla es directamente calculado a partir del tamaño del fondo,
			// esta es una situación imposible, la aparición de este mensaje indica un ERROR GRAVE.
	this->areaBackground.x = (W - this->areaDrawable.w) / 2;
	this->areaBackground.y = (H - this->areaDrawable.h) / 2;
	this->areaBackground.w = this->areaDrawable.w;
	this->areaBackground.h = this->areaDrawable.h;
}

void clasesGlobales::Pantalla::State::setGUI(clasesGlobales::Pantalla *screen){
	SDL_RenderSetLogicalSize(screen->renderer, this->areaDrawable.w, this->areaDrawable.h);
	SDL_SetRenderDrawColor(screen->renderer, defaultBackground.R, defaultBackground.G, defaultBackground.B, defaultBackground.A);
}


void clasesGlobales::Pantalla::State::draw(clasesGlobales::Pantalla *screen){
depuracion("Entrando Pantalla::State::draw.\n");
	SDL_RenderClear(screen->renderer);
	SDL_RenderCopy(screen->renderer, this->background(), &(this->areaBackground), &(this->areaDrawable));
	SDL_RenderPresent(screen->renderer);
depuracion("Saliendo Pantalla::State::draw.\n");
}

SDL_Rect &clasesGlobales::Pantalla::State::getAreaDrawable(){ return this->areaDrawable;}
