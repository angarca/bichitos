.PHONY: todo libs exe setuplogs depurar

logs = log.txt
INCLUDES = -I$(SDLINCLUDEPATH) -I$(MIINCLUDE)
LIBS = -L$(SDLLIBPATH) -lSDL2 -lSDL2_image -lpthread -D_REENTRANT -L$(MILIB) -lmiLibs

todo: libs main

libs:
	make -C $(MILIBS) String RWops Texture

exe: todo
	-./main

setuplogs:
	echo "#define depurar" > depuracion.h

depurar: setuplogs exe
	echo > depuracion.h
	cat $(logs)

objetos :=

cabeceras.h: ./*.h $(MILIB)/libmiLibs.a
	touch cabeceras.h

objetos := $(objetos) modeloJuego.o
modeloJuego.o: modeloJuego.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) modeloJuego.cpp -o modeloJuego.o

objetos := $(objetos) modeloMapa.o
modeloMapa.o: modeloMapa.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) modeloMapa.cpp -o modeloMapa.o

objetos := $(objetos) modeloPresentacion.o
modeloPresentacion.o: modeloPresentacion.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) modeloPresentacion.cpp -o modeloPresentacion.o

objetos := $(objetos) modelo.o
modelo.o: modelo.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) modelo.cpp -o modelo.o

objetos := $(objetos) dibujadorResumen.o
dibujadorResumen.o: dibujadorResumen.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) dibujadorResumen.cpp -o dibujadorResumen.o

objetos := $(objetos) dibujadorJuego.o
dibujadorJuego.o: dibujadorJuego.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) dibujadorJuego.cpp -o dibujadorJuego.o

objetos := $(objetos) dibujadorMapa.o
dibujadorMapa.o: dibujadorMapa.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) dibujadorMapa.cpp -o dibujadorMapa.o

objetos := $(objetos) dibujadorPresentacion.o
dibujadorPresentacion.o: dibujadorPresentacion.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) dibujadorPresentacion.cpp -o dibujadorPresentacion.o

objetos := $(objetos) dibujadorState.o
dibujadorState.o: dibujadorState.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) dibujadorState.cpp -o dibujadorState.o

objetos := $(objetos) dibujador.o
dibujador.o: dibujador.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) dibujador.cpp -o dibujador.o

objetos := $(objetos) encolador.o
encolador.o: encolador.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) encolador.cpp -o encolador.o

objetos := $(objetos) despachador.o
despachador.o: despachador.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) despachador.cpp -o despachador.o

objetos := $(objetos) main.o
main.o: main.cpp cabeceras.h
	g++ -std=c++11 -c $(INCLUDES) main.cpp -o main.o

main: $(objetos)
	g++ $(objetos) -o main $(LIBS)

