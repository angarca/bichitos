#ifndef dibujador_H
#define dibujador_H

namespace clasesGlobales{
	struct Pantalla{
		struct Color{ Uint32 R,G,B,A; Color(Uint32 r, Uint32 g, Uint32 b, Uint32 a):R(r),G(g),B(b),A(a){}};
		static const Color defaultBackground;
		Sint32 W,H;
		SDL_Window *ventana;
		SDL_Renderer *renderer;
		Modelo *modelo;
		Pantalla(Modelo *m);
		~Pantalla();
		class State{
		protected:
			SDL_Rect areaDrawable, areaBackground;
			Texture background;
		public:
			virtual void loadFiles( Pantalla *) = 0;
			virtual void calcAreas( Pantalla *screen);
			virtual void setGUI( Pantalla *screen);
			virtual void draw( Pantalla *screen);
			virtual SDL_Rect &getAreaDrawable();
		};
		class EnPresentacion: public State{
		public:
			EnPresentacion (Pantalla *pantalla);
			~EnPresentacion();
			virtual void loadFiles( Pantalla *pantalla);
		}*enPresentacion;
		class EnMapa: public State{
		protected:
			Array<Texture> pantallas;
		public:
			EnMapa(Pantalla *pantalla);
			~EnMapa();
			virtual void loadFiles(Pantalla *pantalla);
			virtual void draw(Pantalla *pantalla);
		}*enMapa;
		class EnJuego: public State{
		protected:
			SDL_Rect areaJugable;
			Array<Texture> paleta;
		public:
			EnJuego(Pantalla *pantalla);
			~EnJuego();
			virtual void loadFiles(Pantalla *pantalla);
			virtual void calcAreas(Pantalla *pantalla);
			virtual void draw(Pantalla *pantalla);
			virtual SDL_Rect &getAreaJugable();
		}*enJuego;
		class EnResumen: public State{
		public:
			EnResumen(Pantalla *pantalla);
			~EnResumen();
			void loadFiles(Pantalla *pantalla);
		}*enResumen;
	};
}

namespace funcionesGlobales{
	void dibujadorEnJuego(void *param);
}

#endif//dibujador_H
