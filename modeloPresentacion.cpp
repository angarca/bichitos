#include "cabeceras.h"

const String clasesGlobales::Modelo::EnPresentacion::configuracionName = "configuracion.txt";
const String &clasesGlobales::Modelo::EnPresentacion::AssetPath = clasesGlobales::Modelo::AssetPath;

clasesGlobales::Modelo::EnPresentacion::EnPresentacion(clasesGlobales::Modelo *modelo){
	bool salida = false;
	RWops configuracion(modelo->AssetPath + configuracionName);
	while(!salida)
		salida = load( configuracion.readLine().split());
}

clasesGlobales::Modelo::EnPresentacion::~EnPresentacion(){}

bool clasesGlobales::Modelo::EnPresentacion::load(const Array<String> &line){
	bool ret = false;
	if( line[0] == "pantallaPresentacion" )
		this->fondo = this->AssetPath + line[1];
	else if( line[0] == "mapa" )
		this->mapaName = this->AssetPath + line[1];
	else if( line[0] == "fin" )
		ret = true;
//	else		// Identificador no reconocido, ignorar.
	return ret;
}
