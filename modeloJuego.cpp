#include "cabeceras.h"

clasesGlobales::Modelo::EnJuego::EnJuego(clasesGlobales::Modelo *modelo, const String &fileName):puntuacion(0){
	Sint32 i,j,k;	bool salida = false;
	Paleta tmp;
	String paletaName, gridName, cad;
	RWops file( fileName);	Array<String> line;
	while(!salida){
		line = file.readLine().split();
		if(line[0] == "fondo")
			this->fondo = modelo->AssetPath + line[1];
		else if(line[0] == "paleta")
			paletaName = modelo->AssetPath + line[1];
		else if(line[0] == "grid")
			gridName = modelo->AssetPath + line[1];
		else if(line[0] == "resumen")
			this->resumen = modelo->AssetPath + line[1];
		else if(line[0] == "fin") salida = true;
	}
	file.open(paletaName);	salida = false;
	this->umbralAparicion = 0;
	while(!salida){
		line = file.readLine().split();
		if(line[0] == "fin") salida = true;
		else if(line[0] == "resolucion"){
			this->TW = line[1].to_i();
			this->TH = line[2].to_i();
		}else if(!(line[0] == "" || line[1] == "")){
			tmp.seleccionable = false;
			tmp.caible = false;
			tmp.Animacion = 1;	tmp.a = 0;
			tmp.Estados = 1;
			tmp.umbralAparicion = 0;
			tmp.identificador = line[0];
			tmp.imageName = modelo->AssetPath + line[1];
			for(i=2; i <= line.getLength(); i++){
				if(line[i] == "S") tmp.seleccionable = true;
				else if(line[i] == "C") tmp.caible = true;
				else if(line[i] == "A")
					tmp.Animacion = line[++i].to_i();
				else if(line[i] == "E")
					tmp.Estados = line[++i].to_i();
				else if(line[i] == "F")
					tmp.umbralAparicion = this->umbralAparicion += line[++i].to_i();
			}
			this->paleta.add(tmp);
		}
	}
	file.open(gridName);	salida = false;
	line = file.readLine().split();
	this->W = line[0].to_i();	this->H = line[1].to_i();
	this->grid = new Sint32*[this->W];
	this->floor = new Sint32*[this->W];
	this->seleccionados = new EstadoSeleccion*[this->W];
	for(i=0; i<this->W; i++)	this->grid[i] = new Sint32[this->H];
	for(i=0; i<this->W; i++)	this->floor[i] = new Sint32[this->H];
	for(i=0; i<this->W; i++)	this->seleccionados[i] = new EstadoSeleccion[this->H];
	for(salida = false, j=0; !salida && j<this->H; j++){
		for(i=0; !salida && i<this->W;){
			cad = file.readLine(String(" \t\n\r"));
			if(cad == "fin") salida = true;
			else if(!(cad == "")){
				k = this->paleta.find<String>( &Paleta::amI, cad);
				this->grid[i][j] = k;
				this->seleccionados[i][j].seleccionado = false;
				i++;
			}
		}
	}
/*	if(i < this->W || j < this->H)
		throw "Error, Modelo::EnJuego::EnJuego: el fichero de grid esta incompleto.\n";*/
}

clasesGlobales::Modelo::EnJuego::~EnJuego(){
	Sint32 i;
	for(i=0; i<this->W; i++){
		delete[] this->grid[i];
		delete[] this->floor[i];
		delete[] this->seleccionados[i];
	}
	delete[] this->grid;
	delete[] this->floor;
	delete[] this->seleccionados;
	SDL_RemoveTimer(this->timerID);
}
