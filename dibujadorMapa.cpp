#include "cabeceras.h"

clasesGlobales::Pantalla::EnMapa::EnMapa(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando Pantalla::EnMapa::EnMapa.\n");
	this->loadFiles(pantalla);
	this->calcAreas(pantalla);
	this->setGUI(pantalla);
	this->draw(pantalla);
depuracion("Saliendo Pantalla::EnMapa::EnMapa.\n");
}

clasesGlobales::Pantalla::EnMapa::~EnMapa(){
depuracion("Entrando Pantalla::EnMapa::~EnMapa.\n");
depuracion("Saliendo Pantalla::EnMapa::~EnMapa.\n");
}

void clasesGlobales::Pantalla::EnMapa::loadFiles(clasesGlobales::Pantalla *pantalla){
	Sint32 i,t;
	this->background.createTexture( pantalla->renderer, pantalla->modelo->enMapa->fondo);
	this->pantallas.setLength( pantalla->modelo->enMapa->pantallas.getLength());
	t = this->pantallas.getLength();
	for(i=0; i < t; i++){
		this->pantallas[i].createTexture( pantalla->renderer, pantalla->modelo->enMapa->pantallas[i].imageName);
	}
}

void clasesGlobales::Pantalla::EnMapa::draw(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando Pantalla::EnMapa::draw.\n");
	Sint32 i, t = this->pantallas.getLength();
	SDL_Rect dst;
	SDL_RenderClear(pantalla->renderer);
	SDL_RenderCopy( pantalla->renderer, this->background(), &(this->areaBackground), &(this->areaDrawable));
	for(i=0; i < t; i++){
		dst.w = this->pantallas[i].getW();	if( dst.w > this->areaDrawable.w) dst.w = this->areaDrawable.w;
		dst.h = this->pantallas[i].getH();	if( dst.h > this->areaDrawable.h / t) dst.h = this->areaDrawable.h / t;
		dst.x = (this->areaDrawable.w - dst.w) / 2;
		dst.y = (i * this->areaDrawable.h / t) + (this->areaDrawable.h / t - dst.h) / 2;
		SDL_RenderCopy(pantalla->renderer, this->pantallas[i](), nullptr, &dst);
	}
	SDL_RenderPresent(pantalla->renderer);
depuracion("Saliendo Pantalla::EnMapa::draw.\n");
}
