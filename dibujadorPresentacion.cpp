#include "cabeceras.h"

clasesGlobales::Pantalla::EnPresentacion::EnPresentacion(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando Pantalla::EnPresentacion::EnPresentacion.\n");
	this->loadFiles(pantalla);
	this->calcAreas(pantalla);
	this->setGUI(pantalla);
	this->draw(pantalla);
depuracion("Saliendo Pantalla::EnPresentacion::EnPresentacion.\n");
}

clasesGlobales::Pantalla::EnPresentacion::~EnPresentacion(){
depuracion("Entrando Pantalla::EnPresentacion::~EnPresentacion.\n");
depuracion("Saliendo Pantalla::EnPresentacion::~EnPresentacion.\n");
}

void clasesGlobales::Pantalla::EnPresentacion::loadFiles(clasesGlobales::Pantalla *pantalla){
	this->background.createTexture( pantalla->renderer, pantalla->modelo->enPresentacion->fondo);
}
