#include "cabeceras.h"

Uint32 funcionesGlobales::encolador(Uint32 interval, void *param){
	SDL_PushEvent((SDL_Event*)param);
	return interval;
}

SDL_Event *funcionesGlobales::encapsularEvento(void (*p)(void*), void *param){
	SDL_Event *evento = new SDL_Event;
	SDL_UserEvent user;

	user.type = SDL_USEREVENT;
	user.code = 0;
	user.data1 = (void*)p;
	user.data2 = param;

	evento->type = SDL_USEREVENT;
	evento->user = user;

	return evento;
}
