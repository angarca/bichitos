#ifndef encolador_H
#define encolador_H

namespace funcionesGlobales{
	Uint32 encolador(Uint32 interval, void *param);
	SDL_Event *encapsularEvento(void(*p)(void*),void*param);
}

#endif//encolador_H
