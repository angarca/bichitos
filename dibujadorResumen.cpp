#include "cabeceras.h"

clasesGlobales::Pantalla::EnResumen::EnResumen(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando Pantalla::EnResumen::EnResumen.\n");
	this->loadFiles(pantalla);
	this->calcAreas(pantalla);
	this->setGUI(pantalla);
	this->draw(pantalla);
depuracion("Saliendo Pantalla::EnResumen::EnResumen.\n");
}

clasesGlobales::Pantalla::EnResumen::~EnResumen(){
depuracion("Entrando Pantalla::EnResumen::~EnResumen.\n");
depuracion("Saliendo Pantalla::EnResumen::~EnResumen.\n");
}

void clasesGlobales::Pantalla::EnResumen::loadFiles(clasesGlobales::Pantalla *pantalla){
	this->background.createTexture( pantalla->renderer, pantalla->modelo->enJuego->resumen);
}
