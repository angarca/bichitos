#include "cabeceras.h"

bool despachadorEnPresentacion(clasesGlobales::Pantalla *pantalla, SDL_Event &evento);
bool despachadorEnMapa(clasesGlobales::Pantalla *pantalla, SDL_Event &evento);
bool despachadorEnJuego(clasesGlobales::Pantalla *pantalla, SDL_Event &evento);
bool despachadorEnResumen(clasesGlobales::Pantalla *pantalla, SDL_Event &evento);

void funcionesGlobales::despachador(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando despachador.\n");
	SDL_Event evento;
	bool salida = false;
	while(!salida){
		if(SDL_WaitEvent(&evento))switch(evento.type){
		case SDL_QUIT:
			salida = true;
			break;
		case SDL_USEREVENT:{
			void (*p)(void*) = (void(*)(void*))evento.user.data1;
			p(evento.user.data2);
			}break;
		case SDL_WINDOWEVENT:
			if(evento.window.event == SDL_WINDOWEVENT_RESIZED)
				SDL_SetWindowSize(pantalla->ventana, evento.window.data1, evento.window.data2);
			break;
		default:
			if(evento.type == SDL_KEYDOWN && evento.key.keysym.sym == SDLK_q) salida = true;
			else switch(pantalla->modelo->estado){
			case clasesGlobales::Modelo::ENPRESENTACION:
				salida = despachadorEnPresentacion(pantalla, evento);
				break;
			case clasesGlobales::Modelo::ENMAPA:
				salida = despachadorEnMapa(pantalla, evento);
				break;
			case clasesGlobales::Modelo::ENJUEGO:
				salida = despachadorEnJuego(pantalla, evento);
				break;
			case clasesGlobales::Modelo::ENRESUMEN:
				salida = despachadorEnResumen(pantalla, evento);
				break;
			default:;
/*				throw "despachador: Estado de modelo no reconocido, no se hace nada.\n";*/
			}
		}
	}
depuracion("Saliendo despachador.\n");
}

bool despachadorEnPresentacion(clasesGlobales::Pantalla *pantalla, SDL_Event &evento){
depuracion("Entrando despachadorEnPresentacion.\n");
	bool salida = false;
	switch(evento.type){
	case SDL_KEYDOWN:
		if(evento.key.keysym.sym == SDLK_AC_BACK) salida = true;
		if(evento.key.keysym.sym != SDLK_RETURN)
			break;
	case SDL_FINGERDOWN:
		if(!pantalla->modelo->enMapa){
			pantalla->modelo->enMapa = new clasesGlobales::Modelo::EnMapa(pantalla->modelo);
			pantalla->enMapa = new clasesGlobales::Pantalla::EnMapa(pantalla);
		}else{
			pantalla->enMapa->setGUI(pantalla);
			pantalla->enMapa->draw(pantalla);
		}
		pantalla->modelo->estado = clasesGlobales::Modelo::ENMAPA;
		break;
	case SDL_APP_WILLENTERFOREGROUND:
		pantalla->enPresentacion->draw(pantalla);
		break;
	default:;
	}
depuracion("Saliendo despachadorEnPresentacion.\n");
	return salida;
}

bool despachadorEnMapa(clasesGlobales::Pantalla *pantalla, SDL_Event &evento){
depuracion("Entrando despachadorEnMapa.\n");
	Sint32 tmp;
	switch(evento.type){
	case SDL_KEYDOWN:
		if(evento.key.keysym.sym == SDLK_AC_BACK || evento.key.keysym.sym == SDLK_BACKSPACE){
			pantalla->modelo->estado = clasesGlobales::Modelo::ENPRESENTACION;
			pantalla->enPresentacion->setGUI(pantalla);
			pantalla->enPresentacion->draw(pantalla);
		}
		if(evento.key.keysym.sym == SDLK_RETURN){
			pantalla->modelo->enJuego = new clasesGlobales::Modelo::EnJuego(pantalla->modelo,
						pantalla->modelo->enMapa->pantallas[0].fileName());
			pantalla->enJuego = new clasesGlobales::Pantalla::EnJuego(pantalla);
			pantalla->modelo->estado = clasesGlobales::Modelo::ENJUEGO;
			pantalla->modelo->enJuego->timerID = SDL_AddTimer( 300, funcionesGlobales::encolador,
				(void*)funcionesGlobales::encapsularEvento(funcionesGlobales::dibujadorEnJuego, pantalla));
		}break;
	case SDL_FINGERDOWN:
		tmp = (Sint32) (evento.tfinger.y * pantalla->modelo->enMapa->pantallas.getLength());
		tmp = (tmp >= pantalla->modelo->enMapa->pantallas.getLength()) ?
						(pantalla->modelo->enMapa->pantallas.getLength() - 1) : tmp;
		pantalla->modelo->enJuego = new clasesGlobales::Modelo::EnJuego(pantalla->modelo,
						pantalla->modelo->enMapa->pantallas[tmp].fileName());
		pantalla->enJuego = new clasesGlobales::Pantalla::EnJuego(pantalla);
		pantalla->modelo->estado = clasesGlobales::Modelo::ENJUEGO;
		pantalla->modelo->enJuego->timerID = SDL_AddTimer( 300, funcionesGlobales::encolador,
	(void*)funcionesGlobales::encapsularEvento(funcionesGlobales::dibujadorEnJuego, pantalla));
		break;
	case SDL_APP_WILLENTERFOREGROUND:
		pantalla->enMapa->draw(pantalla);
		break;
	default:;
	}
depuracion("Saliendo despachadorEnMapa.\n");
	return false;
}

bool despachadorEnJuego(clasesGlobales::Pantalla *pantalla, SDL_Event &evento){
depuracion("Entrando despachadorEnJuego.\n");
	clasesGlobales::Modelo::EnJuego::EstadoSeleccion **&seleccionados = pantalla->modelo->enJuego->seleccionados;
	Array<clasesGlobales::Modelo::EnJuego::Paleta> &paleta = pantalla->modelo->enJuego->paleta;
	Sint32 **&grid = pantalla->modelo->enJuego->grid;
	Sint32 &W = pantalla->modelo->enJuego->W;
	Sint32 &H = pantalla->modelo->enJuego->H;
	SDL_Rect &areaDibujable = pantalla->enJuego->getAreaDrawable();
	SDL_Rect &areaJugable = pantalla->enJuego->getAreaJugable();
	static bool pulsado = false, salida = false;
	static Sint32 ultimaX, ultimaY;
	static SDL_FingerID dedo;
	Sint32 x,y,tmp, **gridT, i,j;
	switch(evento.type){
	case SDL_KEYDOWN:
		if(evento.key.keysym.sym == SDLK_AC_BACK || evento.key.keysym.sym == SDLK_BACKSPACE){
			SDL_RemoveTimer(pantalla->modelo->enJuego->timerID);
			delete pantalla->enJuego;	pantalla->enJuego = nullptr;
			delete pantalla->modelo->enJuego;	pantalla->modelo->enJuego = nullptr;
			pantalla->modelo->estado = clasesGlobales::Modelo::ENMAPA;
			pantalla->enMapa->setGUI(pantalla);
			pantalla->enMapa->draw(pantalla);
		}else if(evento.key.keysym.sym == SDLK_RETURN) pantalla->modelo->enJuego->puntuacion = 50;
		break;
	case SDL_FINGERDOWN:if(!pulsado){
			dedo = evento.tfinger.fingerId;
			x = (Sint32)(evento.tfinger.x * areaDibujable.w);
			y = (Sint32)(evento.tfinger.y * areaDibujable.h);
			if(x >= areaJugable.x && x < areaJugable.x + areaJugable.w &&
					y >= areaJugable.y && y < areaJugable.y + areaJugable.h){
				x = (x - areaJugable.x) * W / areaJugable.w;
				y = (y - areaJugable.y) * H / areaJugable.h;
				if(grid[x][y] >= 0 && paleta[grid[x][y]].seleccionable){
					seleccionados[x][y].seleccionado = true;
					seleccionados[x][y].orden = 0;
					ultimaX = x; ultimaY = y;
					pulsado = true;
				}
			}
		}break;
	case SDL_FINGERMOTION:if(pulsado && dedo == evento.tfinger.fingerId){
			x = (Sint32)(evento.tfinger.x * areaDibujable.w);
			y = (Sint32)(evento.tfinger.y * areaDibujable.h);
			x = (x - areaJugable.x) * W / areaJugable.w;
			y = (y - areaJugable.y) * H / areaJugable.h;
			if(x < 0) x = 0; if(x >= W) x = W-1; if(y < 0) y = 0; if(y >= H) y = H-1;
			if(x - ultimaX <= 1 && ultimaX - x <= 1 && y - ultimaY <= 1 && ultimaY - y <= 1)
			if(grid[x][y] == grid[ultimaX][ultimaY])
			if(x != ultimaX || y != ultimaY) if(! seleccionados[x][y].seleccionado){
				for(i=0; i<W; i++)for(j=0; j<H; j++)if(seleccionados[i][j].seleccionado)seleccionados[i][j].orden++;
				ultimaX = x; ultimaY = y;
				seleccionados[x][y].seleccionado = true;
				seleccionados[x][y].orden = 0;
			}else{	// seleccionados[x][y].seleccionado
				if(seleccionados[x][y].orden == 1){
					seleccionados[ultimaX][ultimaY].seleccionado = false;
					for(i=0;i<W;i++)for(j=0;j<H;j++)if(seleccionados[i][j].seleccionado)seleccionados[i][j].orden--;
					ultimaX = x; ultimaY = y;
				}
			}
		}break;
	case SDL_FINGERUP:if(pulsado && dedo == evento.tfinger.fingerId){
			tmp = 0;
			for(x=0;x<W;x++)for(y=0;y<H;y++)if(seleccionados[x][y].seleccionado)
				tmp++;
			if(tmp >= 3){
				for(x=0;x<W;x++)for(y=0;y<H;y++)if(seleccionados[x][y].seleccionado){
					grid[x][y] = -1;	pantalla->modelo->enJuego->puntuacion++;
				}
			}
			for(x=0;x<W;x++)for(y=0;y<H;y++)seleccionados[x][y].seleccionado = false;
			if(tmp >= 3){
				pantalla->enJuego->draw(pantalla);
				do{
					salida = true;
					gridT = new Sint32*[W];
					for(x=0;x<W;x++){
						gridT[x] = new Sint32[H];
						for(y=0;y<H;y++) gridT[x][y] = grid[x][y];
					}
					SDL_Delay(300);
					for(x=0;x<W;x++)if(gridT[x][0]==-1 && pantalla->modelo->enJuego->umbralAparicion > 0){
						salida = false;
						i = SDL_GetTicks() % pantalla->modelo->enJuego->umbralAparicion;
						for(j=0; grid[x][0] == -1 && j < pantalla->modelo->enJuego->paleta.getLength(); j++)
							if(i < pantalla->modelo->enJuego->paleta[j].umbralAparicion)
								grid[x][0] = j;
					}
					for(x=0;x<W;x++)for(y=0;y<H-1;y++){
						if(gridT[x][y+1]==-1 && gridT[x][y]!=-1 && pantalla->modelo->enJuego->paleta[gridT[x][y]].caible){
							salida = false;
							grid[x][y+1] = gridT[x][y];
							grid[x][y] = -1;
						}
					}
					pantalla->enJuego->draw(pantalla);
					for(x=0;x<W;x++)delete[] gridT[x];
					delete[] gridT;
				}while(!salida);	salida = false;
			}
			SDL_FlushEvent(SDL_FINGERDOWN);
			SDL_FlushEvent(SDL_USEREVENT);
			pulsado = false;
		}break;
	case SDL_APP_WILLENTERFOREGROUND:
		pantalla->enJuego->draw(pantalla);
		break;
	default:;
	}

	if(pantalla->modelo->enJuego && pantalla->modelo->enJuego->puntuacion >= 50){
		SDL_RemoveTimer(pantalla->modelo->enJuego->timerID);
		delete pantalla->enJuego;	pantalla->enJuego = nullptr;
		pantalla->enResumen = new clasesGlobales::Pantalla::EnResumen(pantalla);
		pantalla->modelo->estado = clasesGlobales::Modelo::ENRESUMEN;
	}
depuracion("Saliendo despachadorEnJuego.\n");
	return salida;
}

bool despachadorEnResumen(clasesGlobales::Pantalla *pantalla, SDL_Event &evento){
depuracion("Entrando despachadorEnResumen.\n");
	bool salida = false;
	switch(evento.type){
	case SDL_KEYDOWN:
		if(evento.key.keysym.sym != SDLK_AC_BACK && evento.key.keysym.sym != SDLK_RETURN)
			break;
	case SDL_FINGERDOWN:
		delete pantalla->enResumen;	pantalla->enResumen = nullptr;
		delete pantalla->modelo->enJuego;	pantalla->modelo->enJuego = nullptr;
		pantalla->modelo->estado = clasesGlobales::Modelo::ENMAPA;
		pantalla->enMapa->setGUI(pantalla);
		pantalla->enMapa->draw(pantalla);
		break;
	case SDL_APP_WILLENTERFOREGROUND:
		pantalla->enResumen->draw(pantalla);
		break;
	default:;
	}
depuracion("Saliendo despachadorEnResumen.\n");
	return salida;
}

