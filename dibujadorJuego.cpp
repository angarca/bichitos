#include "cabeceras.h"

clasesGlobales::Pantalla::EnJuego::EnJuego(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando Pantalla::EnJuego::EnJuego.\n");
	this->loadFiles(pantalla);
	this->calcAreas(pantalla);
	this->setGUI(pantalla);
	this->draw(pantalla);
depuracion("Saliendo Pantalla::EnJuego::EnJuego.\n");
}

clasesGlobales::Pantalla::EnJuego::~EnJuego(){
depuracion("Entrando Pantalla::EnJuego::~EnJuego.\n");
depuracion("Saliendo Pantalla::EnJuego::~EnJuego.\n");
}

void clasesGlobales::Pantalla::EnJuego::loadFiles(clasesGlobales::Pantalla *pantalla){
	Sint32 i, t = pantalla->modelo->enJuego->paleta.getLength();
	this->background.createTexture( pantalla->renderer, pantalla->modelo->enJuego->fondo);
	this->paleta.setLength( t);
	for(i=0; i < t; i++)
		this->paleta[i].createTexture( pantalla->renderer, pantalla->modelo->enJuego->paleta[i].imageName);
}

void clasesGlobales::Pantalla::EnJuego::calcAreas(clasesGlobales::Pantalla *pantalla){
	this->areaJugable.x = this->areaJugable.y = 0;
	this->areaJugable.w = pantalla->modelo->enJuego->W * pantalla->modelo->enJuego->TW;
	this->areaJugable.h = pantalla->modelo->enJuego->H * pantalla->modelo->enJuego->TH;
	this->areaDrawable.x = this->areaDrawable.y = 0;
	if( pantalla->H * this->areaJugable.w > pantalla->W * this->areaJugable.h){
		this->areaDrawable.w = this->areaJugable.w;
		this->areaDrawable.h = pantalla->H * this->areaJugable.w / pantalla->W;
		this->areaJugable.y = (this->areaDrawable.h - this->areaJugable.h) / 2;
	}else{
		this->areaDrawable.w = pantalla->W * this->areaJugable.h / pantalla->H;
		this->areaDrawable.h = this->areaJugable.h;
		this->areaJugable.x = (this->areaDrawable.w - this->areaJugable.w) / 2;
	}
/*	if( this->background.getW() < this->areaDrawable.w || this->background.getH() < this->areaDrawable.h)
		throw "Error, Pantalla::EnJuego::calcAreas: the size of the background isn't big enougth to areaDrawable.\n";*/
	this->areaBackground.x = (this->background.getW() - this->areaDrawable.w) / 2;
	this->areaBackground.y = (this->background.getH() - this->areaDrawable.h) / 2;
	this->areaBackground.w = this->areaDrawable.w;
	this->areaBackground.h = this->areaDrawable.h;
}

void clasesGlobales::Pantalla::EnJuego::draw(clasesGlobales::Pantalla *pantalla){
depuracion("Entrando Pantalla::EnJuego::draw.\n");
	Sint32 i,j,k, l,m,n;	SDL_Rect org, dst = { 0,0, pantalla->modelo->enJuego->TW, pantalla->modelo->enJuego->TH};
	SDL_RenderClear(pantalla->renderer);
	SDL_RenderCopy(pantalla->renderer, this->background(), &(this->areaBackground), &(this->areaDrawable));
	for( i=0; i<pantalla->modelo->enJuego->W; i++){
		dst.x = i * dst.w + this->areaJugable.x;
		for( j=0; j<pantalla->modelo->enJuego->H; j++){
			dst.y = j * dst.h + this->areaJugable.y;
			k = pantalla->modelo->enJuego->grid[i][j];
			if(k >= 0){
				org.w = this->paleta[k].getW() / pantalla->modelo->enJuego->paleta[k].Animacion;
				org.h = this->paleta[k].getH() / pantalla->modelo->enJuego->paleta[k].Estados;
				org.x = pantalla->modelo->enJuego->paleta[k].a * org.w;
				if(! pantalla->modelo->enJuego->seleccionados[i][j].seleccionado)
					org.y = 0;
				else if(pantalla->modelo->enJuego->paleta[k].Estados >= 2)
					org.y = org.h;
				else{
					org.y = 0;
/*					throw "Warning, Pantalla::EnJuego::draw: seleccionado bichito con un solo estado.\n";*/
				}
				SDL_RenderCopy(pantalla->renderer, this->paleta[k](), &org, &dst);
			}
		}
	}
	SDL_RenderPresent(pantalla->renderer);
	for(i = 0; i<pantalla->modelo->enJuego->paleta.getLength(); i++){
		pantalla->modelo->enJuego->paleta[i].a++;
		if(pantalla->modelo->enJuego->paleta[i].a >= pantalla->modelo->enJuego->paleta[i].Animacion)
			pantalla->modelo->enJuego->paleta[i].a = 0;
	}
depuracion("Saliendo Pantalla::EnJuego::draw.\n");
}

SDL_Rect &clasesGlobales::Pantalla::EnJuego::getAreaJugable(){ return this->areaJugable;}
