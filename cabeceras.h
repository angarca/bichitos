#ifndef cabeceras_H
#define cabeceras_H

//#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

#include <ANG/Array.hpp>
#include <ANG/String.h>
#include <ANG/SDL/RWops.h>
#include <ANG/SDL/Texture.h>

#ifdef __ANDROID_API__
#define Array ANG::Array
#else//__ANDROID_API__
template<class T, int i = 30>
using Array = ANG::Array<T,i>;
#endif//__ANDROID_API__

using ANG::String;
using ANG::SDL::RWops;
using ANG::SDL::Texture;

#include "depuracion.h"

#include "modelo.h"
#include "dibujador.h"
#include "despachador.h"
#include "encolador.h"
//#include "auxiliar.h"

namespace Depuracion{
	inline char *generarNombre(){
		Sint32 i;
		char *name, *path, bar[2];
#ifdef __ANDROID_API__
		path = (char*)SDL_AndroidGetExternalStoragePath();
		bar[0] = '/'; bar[1] = '\0';
#else//__ANDROID_API__
		path = new char[1]; path[0] = '\0';
		bar[0] = '\0';
#endif//__ANDROID_API__
		for(i=0; path[i]!='\0'; i++);	name = new char[i+10];
		sprintf(name, "%s%s%s", path, bar, "log.txt");
#ifndef __ANDROID_API__
		delete[] path;
#endif//__ANDROID_API__
		return name;
	}

	inline void iniciarLog(){
		char *name = generarNombre();
		SDL_RWclose(SDL_RWFromFile(name,"w"));
	}

	inline void depuracion(const char *cad){
#ifdef depurar
		Sint32 i; char *name = generarNombre();
		SDL_RWops *log = SDL_RWFromFile(name,"a");
		delete[] name;
		for(i=0; cad[i]!='\0'; i++);
		SDL_RWwrite(log, cad, sizeof(char), i);
		SDL_RWclose(log);
#endif//depurar
	}
}using Depuracion::depuracion;


#endif//cabeceras_H
